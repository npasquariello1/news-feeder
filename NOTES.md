Features of the app include a delete button which stores the deleted id in the local storage so when user refreshes, the deleted item remains deleted.

A potential issue of the way I iterate through the local storage is if the user deletes the first 'news-feed-card-1' string, the app will not continue to iterate through other deleted items. I didn't forsee this being a big issue, since people usually clear browser data all at once instead of individually.

Given more time, I would have like to add a favorite button that gets saved in the local storage, a filter/sort component, and an "Are you sure you want to delete?" modal when delete item is clicked. I would have liked to have added more keys from the data, but I was unsure what some of them meant, for example 'mod_7_days'. I purposely didn't add any of the 'thumb_url_default' key to click events, since they just took user to thumbnail of image. Hope you like the app, please let me know if you have any further questions.
