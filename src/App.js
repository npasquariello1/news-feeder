import './App.css';
import CardContainer from './components/CardContainer';
import NavBar from './components/NavBar';

function App() {
  return (
    <div className="App">
      <NavBar />
      <CardContainer />
    </div>
  );
}

export default App;
