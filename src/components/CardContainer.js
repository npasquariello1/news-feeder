import React, { Component } from 'react';
import Card from './Card';
import styled from 'styled-components';
import { data } from '../data/data';

const CardContainerWrapper = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 80px 30px;
`;

export default class CardContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
    };
  }

  componentDidMount() {
    const items = data.items;
    items.map(
      (item, index) => (item.unique_id = `news-feed-card-${index + 1}`)
    );
    const storageIndex = this.getNewLocalStorageIdex();
    for (let i = 1; i < storageIndex; i++) {
      let itemIndex = items.findIndex(
        (item) => item.unique_id === localStorage.getItem(`deletedItem${i}`)
      );
      items.splice(itemIndex, 1);
    }
    this.setState({ items });
  }

  handleCardDelete = (unique_id) => {
    const index = this.getNewLocalStorageIdex();
    localStorage.setItem(`deletedItem${index}`, unique_id);

    const items = this.state.items;
    const itemIndex = items.findIndex((item) => item.unique_id === unique_id);
    items.splice(itemIndex, 1);
    this.setState({ items });
  };

  getNewLocalStorageIdex = () => {
    let i = 0;
    let index;
    do {
      i++;
      index = !!localStorage.getItem(`deletedItem${i}`);
    } while (index);
    return i;
  };

  render() {
    return (
      <CardContainerWrapper>
        {this.state.items.map((item, index) => (
          <Card
            item={item}
            deleteCard={this.handleCardDelete}
            key={`news-feed-card-${index + 1}`}
          />
        ))}
      </CardContainerWrapper>
    );
  }
}
