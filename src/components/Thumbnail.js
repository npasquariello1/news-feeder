import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import PlayIcon from '../icons/play-arrow.png';
import UserPlaceholder from '../icons/user_placeholder.png';

const ThumbnailWrapper = styled.div`
  img {
    max-width: 100%;
    display: block;
    object-fit: cover;
  }

  .user__image {
    border-radius: 50%;
  }

  .play__icon {
    position: relative;
    bottom: 75px;
    margin-bottom: -75px;
    cursor: pointer;
  }
`;

export default class Thumbnail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
    };
  }

  render() {
    const { thumbnail, entityType } = this.props;
    const { error } = this.state;
    return (
      <ThumbnailWrapper>
        <img
          src={error || !thumbnail ? UserPlaceholder : thumbnail}
          onError={() => this.setState({ error: true })}
          width="75"
          height="75"
          alt="User Image"
          className="user__image"
        />
        {entityType === 'video' && (
          <img
            width="75"
            height="75"
            className="play__icon"
            src={PlayIcon}
            alt="Play Button"
          />
        )}
      </ThumbnailWrapper>
    );
  }
}

Thumbnail.propTypes = {
  thumbnail: PropTypes.string,
  entityType: PropTypes.string,
};
