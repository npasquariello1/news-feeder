import React, { useEffect, useState } from 'react';

const NavBar = () => {
  const [show, setShow] = useState(true);
  const controlNavBar = () => {
    if (window.scrollY > 100) {
      setShow(false);
    } else {
      setShow(true);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', controlNavBar);
    return () => {
      window.removeEventListener('scroll', controlNavBar);
    };
  }, []);
  return (
    <div className={`nav ${!show && 'display-none'}`}>
      <h1 className={`${!show && 'display-none'}`}>Studio</h1>
      <img
        src="https://s3.amazonaws.com/assets.studio71.io/img/s71_logo512.png"
        width="45"
        height="45"
      />
    </div>
  );
};

export default NavBar;
