import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import DeleteIcon from '../icons/icons8-delete-24.png';
import Thumbnail from './Thumbnail';

const CardWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  max-width: 1200px;
  margin-block: 1rem;

  .card {
    display: flex;
    flex-direction: column;
    width: inherit;
    border-radius: 1em;
    background: #cdcdcd;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  }

  .card__body {
    padding: 0rem 1rem 0rem 1rem;
  }

  .tag {
    align-self: flex-start;
    padding: 0.35rem 0.75rem;
    border-radius: 1em;
    font-size: 0.9rem;
    font-weight: bold;
  }

  .tag + .tag {
    margin-left: 6px;
  }

  .tag-purple {
    background: #420039;
    color: #fafafa;
  }

  .tag-navy {
    background: #00308f;
    color: #fafafa;
  }

  .card__header {
    display: flex;
    padding: 1rem;
    background: #fafafa;
    border-radius: 1em;
  }

  .card__options {
    margin-left: auto;
    display: flex;
    align-items: flex-start;
  }

  .card__header > button {
    margin-left: auto;
  }

  .card__footer {
    display: flex;
    padding: 1rem;
    margin-top: auto;
  }

  .user {
    display: flex;
    gap: 0.5rem;
  }

  .user__info {
    padding-left: 12px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
  }

  .user__info > h5 {
    font-size: 20px;
    margin: 12px 0;
  }

  .delete_button {
    position: relative;
    bottom: 2px;
    margin-left: 6px;
    background: #fafafa;
    padding: 0.2rem;
    border: none;
    border-radius: 60%;
  }

  .delete_button:hover {
    background: #cdcdcd;
  }

  .date {
    font-weight: bold;
  }
`;

export default class Card extends Component {
  getTagType = (type) => {
    const tagTypes = {
      'milestone-subs': 'Subscriber Milestone',
      'trending-video': 'Trending Video',
      'stats-trend': 'Trending Stats',
      'channel-overlap': 'Channel Overlap',
      'delta-subs': 'Subscriber Increase',
      'video-objects': 'Top Video',
      'video-topics': 'Topic Video',
      'channel-new': 'New Channel',
    };
    return <span className="tag tag-purple">{tagTypes[type]}</span>;
  };

  getDate = (date) => {
    return new Date(date).toUTCString();
  };

  render() {
    const { item, deleteCard } = this.props;

    return (
      <CardWrapper>
        <div className="card">
          <div className="card__header">
            <div className="user">
              <Thumbnail
                thumbnail={item.thumbnail}
                entityType={item.entity_type}
              />
              <div className="user__info">
                <h5>{item.title}</h5>
                <div className="user__tags">
                  {item.subscribers && (
                    <span className="tag tag-navy">
                      {item.subscribers} Subscribers
                    </span>
                  )}
                  {item.timeframe && (
                    <span className="tag tag-navy">{item.timeframe}</span>
                  )}
                </div>
              </div>
            </div>
            <div className="card__options">
              {this.getTagType(item.type)}
              <button
                className="delete_button"
                onClick={() => deleteCard(item.unique_id)}
              >
                <img src={DeleteIcon} alt="Delete Button" />
              </button>
            </div>
          </div>
          <div className="card__body">
            <p>{item.message}</p>
            {item.added_cms && (
              <p className="date">Added: {this.getDate(item.added_cms)}</p>
            )}
          </div>
        </div>
      </CardWrapper>
    );
  }
}

Card.propTypes = {
  item: PropTypes.shape({
    unique_id: PropTypes.string.isRequired,
    entity_type: PropTypes.string,
    category: PropTypes.string,
    est_subs: PropTypes.string,
    mod_7_days: PropTypes.number,
    subscribers: PropTypes.number,
    name: PropTypes.string,
    cms: PropTypes.string,
    mod_30_days: PropTypes.number,
    type: PropTypes.string.isRequired,
    title: PropTypes.string,
    estimated_subscribers_30_days: PropTypes.number,
    thumbnail: PropTypes.string,
    id: PropTypes.string,
    entity_id: PropTypes.string,
    timeframe: PropTypes.string,
    domain: PropTypes.string,
    estimated_subscribers_7_days: PropTypes.number,
    message: PropTypes.string.isRequired,
    link: PropTypes.string,
    percent_change_30_day: PropTypes.number,
    added_cms: PropTypes.string,
  }),
  deleteCard: PropTypes.func.isRequired,
};
